# Local Dev

Stack for developing in your workstation 

## Docker localhost

This setup start a [traefik](https://traefik.io/) server on host, start a `localhost` docker network and install certificates in order to serve local server with a trusted https address like `https://my-project.docker.localhost`.

The standard way to use it is via [Local Run project](https://gitlab.com/dolmen-public/myawesomemachine/local-ansible-run) via Ansible. However, if you don't want to use this all-in-one tool, here how to do it manually.

1. [Install mkcert and generate certificate](./mkcert/README.md)
2. [Install traefik](./docker/README.md)
