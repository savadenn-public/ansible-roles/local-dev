# Docker traefik

Install a local [traefik](https://traefik.io/) to serve your development containers.

## Manual install

_Requires certificates, see [mkcert](../mkcert/README.md)_

```shell
# Install package to redirect every address *.localhost to 127.0.0.1
sudo apt update && sudo apt install -y libnss-myhostname


# Clone this repo (it will be removed at the end)
git clone https://gitlab.com/dolmen-public/myawesomemachine/roles
cd local-dev

# Copy files
CERTS_PATH=/usr/local/etc/docker/certs
FILES_PATH=/usr/local/etc/docker/
BIN_PATH=/usr/local/bin/localhost
sudo mkdir -p ${FILES_PATH}
sudo cp -R docker/files/* ${FILES_PATH}
sudo cp docker/templates/docker-compose.yml ${FILES_PATH}
sudo chmod -R 644 ${FILES_PATH}
sudo chmod 755 ${FILES_PATH}
sudo cp docker/templates/localhost.sh ${BIN_PATH}
sudo chmod 755 ${BIN_PATH}
sudo mkdir -p ${CERTS_PATH}
sudo chmod 755 ${CERTS_PATH}

# Generate certificate
CAROOT=/usr/local/etc/mkcert
export CAROOT
sudo mkcert -key-file "${CERTS_PATH}/tls.key" -cert-file "${CERTS_PATH}/tls.crt" localhost '*.docker.localhost'

# Replace template
sed -i "s#{{ docker.paths.files }}#${FILES_PATH}#g" ${FILES_PATH}docker-compose.yml
sed -i "s#{{ docker.paths.files }}#${FILES_PATH}#g" ${BIN_PATH}

localhost
```

If all went well you should be able to access Traefik dashboard on [https://localhost](https://localhost)
